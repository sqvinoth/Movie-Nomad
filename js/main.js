//Full width slider 
(function( $ ) {

    //Function to animate slider captions 
    function doAnimations( elems ) {
		//Cache the animationend event in a variable
		var animEndEv = 'webkitAnimationEnd animationend';
		
		elems.each(function () {
			var $this = $(this),
				$animationType = $this.data('animation');
			$this.addClass($animationType).one(animEndEv, function () {
				$this.removeClass($animationType);
			});
		});
	}
	
	//Variables on page load 
	var $myCarousel = $('#carousel-example-generic'),
		$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Animate captions in first slide on page load 
	doAnimations($firstAnimatingElems);
	
	//Pause carousel  
	$myCarousel.carousel('pause');
	
	
	//Other slides to be animated on carousel slide event 
	$myCarousel.on('slide.bs.carousel', function (e) {
		var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
		doAnimations($animatingElems);
	});  
    $('#carousel-example-generic').carousel({
        interval:3000,
        pause: "false"
    });
	
})(jQuery);//Full width slider
	

//js slimscroll 
//$(function(){
//      $('.slimscroll').slimScroll({
//          allowPageScroll: true
//      });
//
//});
//js slimscroll 

//favorit icon animation effect script
$(document).ready(function()
	{
    
	$('body').on("click",'.sub-page-title-set-favorit-heart',function()
    {
    	
    	var A=$(this).attr("id");
    	var B=A.split("like");
        var messageID=B[1];
        var C=parseInt($("#likeCount"+messageID).html());
    	$(this).css("background-position","")
        var D=$(this).attr("rel");
       
        if(D === 'like') 
        {      
        $("#likeCount"+messageID).html(C+1);
        $(this).addClass("heartAnimation").attr("rel","unlike");
        
        }
        else
        {
        $("#likeCount"+messageID).html(C-1);
        $(this).removeClass("heartAnimation").attr("rel","like");
        $(this).css("background-position","left");
        }


    });

});


$(document).ready(function()
	{    
	$('body').on("click",'.track-set-favorit-heart',function()
    {
    	
    	var A=$(this).attr("id");
    	var B=A.split("like");
        var messageID=B[1];
        var C=parseInt($("#likeCount"+messageID).html());
    	$(this).css("background-position","")
        var D=$(this).attr("rel");
       
        if(D === 'like') 
        {      
        $("#likeCount"+messageID).html(C+1);
        $(this).addClass("heartAnimation").attr("rel","unlike");
        
        }
        else
        {
        $("#likeCount"+messageID).html(C-1);
        $(this).removeClass("heartAnimation").attr("rel","like");
        $(this).css("background-position","left");
        }


    });

});
//favorit icon animation effect script

//check list
$(document).ready(function() {

    //When checkboxes/radios checked/unchecked, toggle background color
    $('.form-group').on('click','input[type=radio]',function() {
        $(this).closest('.form-group').find('.radio-inline, .radio').removeClass('checked');
        $(this).closest('.radio-inline, .radio').addClass('checked');
    });
    $('.form-group').on('click','input[type=checkbox]',function() {
        $(this).closest('.checkbox-inline, .checkbox').toggleClass('checked');
    });

    //Show additional info text box when relevant checkbox checked
    $('.additional-info-wrap input[type=checkbox]').click(function() {
        if($(this).is(':checked')) {
            $(this).closest('.additional-info-wrap').find('.additional-info').removeClass('hide').find('input,select').removeAttr('disabled');
        }
        else {
            $(this).closest('.additional-info-wrap').find('.additional-info').addClass('hide').find('input,select').val('').attr('disabled','disabled');
        }
    });

    //Show additional info text box when relevant radio checked
    $('input[type=radio]').click(function() {
        $(this).closest('.form-group').find('.additional-info-wrap .additional-info').addClass('hide').find('input,select').val('').attr('disabled','disabled');
        if($(this).closest('.additional-info-wrap').length > 0) {
            $(this).closest('.additional-info-wrap').find('.additional-info').removeClass('hide').find('input,select').removeAttr('disabled');
        }        
    });
});
//check list


//$(document).ready(function() {
// 
//  $("#favorites-albums-slider,#favorites-playlists-slider").owlCarousel({    
// 
////      autoPlay: 8000, //Set AutoPlay to 3 seconds
//      navigation:true,
//      items : 4,
//      itemsDesktop : [1199,3],
//      itemsDesktopSmall : [979,3],
//      itemsTablet	:[768,3],
//      itemsMobile	:[479,1],
//      navigationText: [
//     "<i class='nav-left-arrow ion-ios-arrow-left'></i>",
//     "<i class='nav-right-arrow ion-ios-arrow-right'></i>"
//     ],
//  });
//});
//
//$(document).ready(function() {
// 
//  $("#favorites-playlists-tracks").owlCarousel({
// 
//      navigation : true, // Show next and prev buttons
//      items : 1,
//      slideSpeed : 300,
//      paginationSpeed : 400,
//      singleItem:true,      
//      itemsDesktop : [1199,1],
//      itemsDesktopSmall : [979,1],
//      itemsTablet	:[768,1],
//      itemsMobile	:[479,1],
//      navigationText: [
//     "<i class='tracks-nav-left-arrow ion-ios-arrow-left'></i>",
//     "<i class='tracks-nav-right-arrow  ion-ios-arrow-right'></i>"
//     ],
//      // "singleItem:true" is a shortcut for:
//      // items : 1, 
//      // itemsDesktop : false,
//      // itemsDesktopSmall : false,
//      // itemsTablet: false,
//      // itemsMobile : false
// 
//  });
// 
//});

//carousel slider script
$(document).ready(function() {
    $('#favorites-albums-slider,#favorites-playlists-slider,#favorites-tracks-slider,#favorites-movies-slider').carousel({
	    interval: false
	})
});//carousel slider script

//Notification script
(function () {
    var bttn = document.getElementById('notification-trigger');

    // make sure..
    bttn.disabled = false;

    bttn.addEventListener('click', function () {
        // simulate loading (for demo purposes only)
        classie.add(bttn, 'active');
        setTimeout(function () {

            classie.remove(bttn, 'active');

            // create the notification
            var notification = new NotificationFx({
                message: '<p>This notification has slight elasticity to it thanks to.</p>',
                layout: 'growl',
                effect: 'slide',
                type: 'notice', // notice, warning or error
                ttl : 900000,
                onClose: function () {
                    bttn.disabled = false;
                }
            });

            // show the notification
            notification.show();

        }, 1200);

        // disable the button (for demo purposes only)
        this.disabled = true;
    });
})();

//Notification script

//Image lightbox script
$(function(){
    var $gallery = $('.gallery a').simpleLightbox();

    $gallery.on('show.simplelightbox', function () {
        console.log('Requested for showing');
    })
            .on('shown.simplelightbox', function () {
                console.log('Shown');
            })
            .on('close.simplelightbox', function () {
                console.log('Requested for closing');
            })
            .on('closed.simplelightbox', function () {
                console.log('Closed');
            })
            .on('change.simplelightbox', function () {
                console.log('Requested for change');
            })
            .on('next.simplelightbox', function () {
                console.log('Requested for next');
            })
            .on('prev.simplelightbox', function () {
                console.log('Requested for prev');
            })
            .on('nextImageLoaded.simplelightbox', function () {
                console.log('Next image loaded');
            })
            .on('prevImageLoaded.simplelightbox', function () {
                console.log('Prev image loaded');
            })
            .on('changed.simplelightbox', function () {
                console.log('Image changed');
            })
            .on('nextDone.simplelightbox', function () {
                console.log('Image changed to next');
            })
            .on('prevDone.simplelightbox', function () {
                console.log('Image changed to prev');
            })
            .on('error.simplelightbox', function (e) {
                console.log('No image found, go to the next/prev');
                console.log(e);
            });
});//Image lightbox script



